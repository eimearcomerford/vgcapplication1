﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VGCApplication1.Startup))]
namespace VGCApplication1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
